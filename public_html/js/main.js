//----------------------------------------------------------------
// >>> TABLE OF CONTENTS:
//----------------------------------------------------------------

// 01. Select List (Dropdown)
// 02. Facts Counter
// 03. Category Filter (MixItUp Plugin)
// 04. 
// 05. 
// 06. 
// 07. 
// 08. 
// 09. 
// 10. 

//----------------------------------------------------------------

( function ($) {
    'use strict';
        
    $(function () {
        
        //Select List (Dropdown)
        //--------------------------------------------------------
        var selectObj = $('select');
        var selectListObj = $('ul.select-list');
        selectObj.each(function(){
            var $this = $(this), numberOfOptions = $(this).children('option').length;

            $this.addClass('select-hidden'); 
            $this.wrap('<div class="select"></div>');
            $this.after('<div class="select-styled"></div>');

            var $styledSelect = $this.next('div.select-styled');
            $styledSelect.text($this.children('option').eq(0).text());

            var $list = $('<ul />', {
                'class': 'select-list'
            }).insertAfter($styledSelect);

            for (var i = 0; i < numberOfOptions; i++) {
                $('<li />', {
                    text: $this.children('option').eq(i).text(),
                    rel: $this.children('option').eq(i).val()
                }).appendTo($list);
            }

            var $listItems = $list.children('li');

            $styledSelect.on('click', function(e) {
                e.stopPropagation();
                $('div.select-styled.active').not(this).each(function(){
                    $(this).removeClass('active').next(selectListObj).hide();
                });
                $(this).toggleClass('active').next(selectListObj).toggle();
            });

            $listItems.on('click', function(e) {
                e.stopPropagation();
                $styledSelect.text($(this).text()).removeClass('active');
                $this.val($(this).attr('rel'));
                $list.hide();
            });

            $(document).on('click', function() {
                $styledSelect.removeClass('active');
                $list.hide();
            });

        });
        
        //Facts Counter
        //--------------------------------------------------------
        var counterObj = $('.fact-counter');
        counterObj.counterUp({
            delay: 10,
            time: 500
        });
        
        //Category Filter (MixItUp Plugin)
        //--------------------------------------------------------
        var folioFilterObj = $('#category-filter');
        folioFilterObj.mixItUp();
        
        //Vertical Tabs
        //--------------------------------------------------------
        $(document).ready(function() {
            $(".tabs-menu a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $(".tabs-list .tab-content").removeClass("active");
                $(".tabs-list .tab-content").eq(index).addClass("active");
            });
        });
        
    });
    
})(jQuery);